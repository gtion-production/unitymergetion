﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mergetion
{
    [ExecuteInEditMode]
    public class MergetionView : MonoBehaviour
    {


        [Header("Fill in")]
        public GameObject prefabsA;
        public GameObject prefabsB;
        public Transform resultHolder;



        [Header("Scan Result")]
        public List<ConflictReport> difference = new List<ConflictReport>();

        [Header("Generated Prefabs")]
        public GameObject resultObject;



        [Header("Get Component Target(TEST)")]
        [Header("_____________________________________________")]
        public GameObject getObject;



        [Header("Action")]
        [Header("_____________________________________________")]

        public bool scan = false;
        public bool implementDiff = false;
        public bool reset = false;
        [Header("_____________________________________________")]
        public bool copyA = false;
        public bool copyB = false;

        MergetionCore core = new MergetionCore();



        private void Update()
        {
            if (copyA)
            {
                copyA = false;
                Copy(true);
            }
            if (copyB)
            {
                copyB = false;
                Copy(false);
            }
            if (scan)
            {
                scan = false;
                Scan();
            }

            if (implementDiff)
            {
                implementDiff = false;
                ImplementDiff();
            }

            if (reset)
            {
                reset = false;
                ResetAll();
            }

        }




        void Copy(bool isA)
        {
            Debug.Log("COPY");
            GameObject temp = isA ? prefabsA : prefabsB;
            if (resultObject != null)
            {
                DestroyImmediate(resultObject);
            }
            resultObject = Instantiate(temp, resultHolder);
            resultObject.name = temp.name;
        }

        void Scan()
        {
            Debug.Log("SCAN");
            difference = core.FindDifference(prefabsA, prefabsB, null, null);
        }

        void ImplementDiff()
        {
            Debug.Log("IMPLEMENT DIFF");
            int len = difference.Count;
            for (int i = len - 1; i >= 0; i--)
            {
                if (!difference[i].doMerge)
                    continue;

                core.SolveConflict(difference[i]);
                difference.RemoveAt(i);
            }
        }

        void ResetAll()
        {
            DestroyImmediate(resultObject);
            difference.Clear();
        }
    }
}
